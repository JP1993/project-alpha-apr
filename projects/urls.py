from django.urls import path
from projects.views import project_list, project_detail, project_create


urlpatterns = [
    path("create/", project_create, name="create_project"),
    path("<int:id>/", project_detail, name="show_project"),
    path("", project_list, name="list_projects"),
]
